# Dual boot system upgrade - chroot
A script to upgrade a (Debian/Ubuntu based) Linux system TOGETHER WITH another existing dual boot system. Useful when you have a rarely used system in dual boot that you don't want to leave it unmaintained for a long period.

### WARNINGS:
1. The script ignores Ubuntu "phased updates" in order to keep the two systems synchronized. This is NOT something that ordinary users should do, because it increases the possibility of a broken system. More info [here](https://askubuntu.com/questions/1431940/what-are-phased-updates-and-why-does-ubuntu-use-them).

2. When kernel updates are available, it is recommended not to do "autoremove" at the same session as "dist-upgrade". Chose "n" to the initial question about automating the installation, chose "n" when the script executes autoremove, and re-run the script after a reboot. Failing to act like this may brake dual booting.

### DISCLAIMER:
This "blind" upgrade method cannot detect some situations when something brakes during the upgrade (in the desktop environment etc, especially because of the 1st warning above). You should boot the target system from time to time in order to check for possible malfunctions.

**Use this at your own risk!**

### Parameters:
Explained in comments inside the "PARAMETERS" section of the script.

### Usage:
Just copy the script to a convenient directory (I use "*/root/scripts/*") and make it executable:
```bash
# Run as root or with sudo.
mkdir -p /root/scripts
cp [/path/to/the/file]/upgrade /root/scripts/upgrade
chown root: /root/scripts/upgrade
chmod 700 /root/scripts/upgrade
```
Then run it like this:
```bash
# Run as root or with sudo.
/root/scripts/upgrade
```
The script can be extended to support multiple boot environments.